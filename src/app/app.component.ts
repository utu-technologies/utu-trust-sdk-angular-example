import { Component } from '@angular/core';
import '@ututrust/web-components';
import { FormBuilder } from '@angular/forms';

import { getToken } from 'utu-sdk-example-common/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  OFFERS = [
    {
      name: 'Paul',
      id: 'provider-1'
    },
    {
      name: 'Jane',
      id: 'provider-2'
    },
    {
      name: 'Ali',
      id: 'provider-3'
    }
  ];

  recommendationIds = ['e541df40-74b6-478e-a7da-7a9e52778700'];
  tokenValue = false;
  apiKeyForm = this.formBuilder.group({
    apiKey: '',
  });
  constructor(
    private formBuilder: FormBuilder,
  ) {}

  onSubmit() {
    console.log(this.apiKeyForm.value);
    getToken(this.apiKeyForm.value.apiKey, 'user-1').then((token: any) => {
      window.dispatchEvent(new CustomEvent('utuIdentityDataReady', { detail: token }));
      this.tokenValue = true;
      console.log(token);
    });
  }


}

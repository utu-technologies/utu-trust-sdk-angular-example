# UTU Trust SDK Angular example app


UTU’s Trust API seamlessly serves up personalized recommendations for trusted service providers on sharing platforms to drive your conversion, satisfaction, retention, and viral acquisition.

[More information about UTU](https://utu.io)

UTU Trust SDK provides web-components that access UTU’s Trust API and allows you simple integration with its services.

Find the [repository here](https://bitbucket.org/utu-technologies/utu-trust-sdk)


## Features

- Integration with UTU's Trust API



## Examples


You can find other working examples in the links below


[react example](https://bitbucket.org/utu-technologies/utu-trust-sdk-react-example/src/master/)

[vue example](https://bitbucket.org/utu-technologies/utu-sdk-vue-example-app/src/master/)

[vanilla javascript example](https://bitbucket.org/utu-technologies/utu-sdk-vanilla-js-example-app/src/master/)



Before running, run `npm install` in the root folders then `npm start`,

### Quickstart Angular

Add `CUSTOM_ELEMENTS_SCHEMA` to module schemas

```js
// app.module.ts
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
```

Import `@ututrust/web-components`. We assume offer ids are in
`offerIds` to your component.

```js
// app.module.ts
import { Component } from "@angular/core";
import "@ututrust/web-components";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  offerIds = ["e541df40-74b6-478e-a7da-7a9e52778700"];
}
```

Add template:

```html
<!-- app.component.html -->
<div class="content" role="main">
  <x-utu-root api-key="[place your utu api key here]">
    <ul>
      <li *ngFor="let offerId of offerIds">
        <x-utu-recommendation [target-uuid]="offerId"></x-utu-recommendation>
      </li>
    </ul>
  </x-utu-root>
</div>
```
